/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.carl.cutimgutil.model;

/**
 * LocalImage
 *
 * @author AnBetter
 * @since 2021-05-11
 */
public final class LocalImage {
    private int cropType;
    private String title;
    private int drawableResource;
    private String imgUrl = "";

    /** LocalImage
     *
     * @param cropType
     * @param title
     * @param drawableResource
     */
    public LocalImage(int cropType, String title, int drawableResource) {
        this.cropType = cropType;
        this.title = title;
        this.drawableResource = drawableResource;
    }

    /** LocalImage
     *
     * @param cropType
     * @param title
     * @param imgUrl
     */
    public LocalImage(int cropType, String title, String imgUrl) {
        this.cropType = cropType;
        this.title = title;
        this.imgUrl = imgUrl;
    }

    /** LocalImage
     *
     * @param cropType
     * @param title
     * @param drawableResource
     * @param imgUrl
     */
    public LocalImage(int cropType, String title, int drawableResource, String imgUrl) {
        this.cropType = cropType;
        this.title = title;
        this.drawableResource = drawableResource;
        this.imgUrl = imgUrl;
    }

    public int getCropType() {
        return cropType;
    }

    public String getTitle() {
        return title;
    }

    public int getDrawableResource() {
        return drawableResource;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setCropType(int cropType) {
        this.cropType = cropType;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDrawableResource(int drawableResource) {
        this.drawableResource = drawableResource;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
