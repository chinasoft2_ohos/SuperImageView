/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.carl.cutimgutil;

/**
 * LocalImage
 *
 * @author AnBetter
 * @since 2021-05-11
 */
public interface NineCropType {
    /**
     * NONE
     */
    int NONE = 9;
    /**
     * TOP_LEFT
     */
    int TOP_LEFT = 0;
    /**
     * TOP
     */
    int TOP = 1;
    /**
     * TOP_RIGHT
     */
    int TOP_RIGHT = 2;
    /**
     * LEFT
     */
    int LEFT = 3;
    /**
     * CENTER
     */
    int CENTER = 4;
    /**
     * RIGHT
     */
    int RIGHT = 5;
    /**
     * BOTTOM_LEFT
     */
    int BOTTOM_LEFT = 6;
    /**
     * BOTTOM
     */
    int BOTTOM = 7;
    /**
     * BOTTOM_RIGHT
     */
    int BOTTOM_RIGHT = 8;
    /**
     * 纵向切割
     */
    int BOAT_TOP = 0;
    /**
     * BOAT_CENTER
     */
    int BOAT_CENTER = 1;
    /**
     * BOAT_BOTTOM
     */
    int BOAT_BOTTOM = 2;
    /**
     * 横向切割
     */
    int MOUNT_LEFT = 0;
    /**
     * MOUNT_CENTER
     */
    int MOUNT_CENTER = 1;
    /**
     * MOUNT_RIGHT
     */
    int MOUNT_RIGHT = 2;

    /**
     * test
     */
    void test();
}