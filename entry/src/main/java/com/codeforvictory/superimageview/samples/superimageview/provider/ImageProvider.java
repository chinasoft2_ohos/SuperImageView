/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.provider;

import com.bumptech.glide.Glide;
import com.codeforvictory.superimageview.samples.superimageview.ResourceTable;
import com.codeforvictory.superimageview.samples.superimageview.shared.NetworkImage;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Image;
import ohos.app.Context;

import java.util.List;

/**
 * ImageProvider
 *
 * @author AnBetter
 * @since 2021-05-11
 */
public class ImageProvider extends BaseItemProvider {
    private static final float RADIOS = 30f;
    private Context context;
    private List<NetworkImage> networkImages;

    /** ImageProvider
     *
     * @param context
     * @param networkImages
     */
    public ImageProvider(Context context,List<NetworkImage> networkImages) {
        this.context = context;
        this.networkImages = networkImages;
    }

    @Override
    public int getCount() {
        return networkImages.size();
    }

    @Override
    public Object getItem(int position) {
        return networkImages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
        ViewHolder viewHolder = null;
        Component component = convertComponent;
        if (component == null) {
            component = LayoutScatter.getInstance(context).parse(
                    ResourceTable.Layout_item_network_rounded_image,
                    null, false);
            viewHolder = new ViewHolder();
            Component itemAvatar = (Image) component.findComponentById(ResourceTable.Id_item_news_image);
            if (itemAvatar instanceof Image) {
                viewHolder.image = (Image) itemAvatar;
            }
            component.setTag(viewHolder);
        } else {
            if (component.getTag() instanceof ViewHolder) {
                viewHolder = (ViewHolder) component.getTag();
            }
        }
        if (viewHolder != null) {
            viewHolder.image.setScaleMode(Image.ScaleMode.STRETCH);
        }
        viewHolder.image.setCornerRadius(RADIOS);
        NetworkImage model = networkImages.get(i);
        refreshImageView(viewHolder.image,model);
        return component;
    }

    private void refreshImageView(Image aiv, NetworkImage model) {
        if (aiv == null) {
            return;
        }
        Glide.with(context)
                .asDrawable()
                .load(model.imageUrl())
                .into(aiv);
    }

    /**
     * ViewHolder
     *
     * @author name
     * @since 2021-04-28
     */
    private static class ViewHolder {
        Image image;
    }
}
