/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.slice;

import com.codeforvictory.superimageview.samples.superimageview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.service.WindowManager;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * ImageProvider
 *
 * @author AnBetter
 * @since 2021-04-25
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private Button mImageCropping;
    private Button mImageCroppingNetwork;
    private Button mRoundedCorners;
    private Button mRoundedCornersImageCropping;
    private final float radios = 30f;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mImageCropping = (Button)findComponentById(ResourceTable.Id_image_cropping);
        mImageCroppingNetwork = (Button)findComponentById(ResourceTable.Id_image_cropping_network);
        mRoundedCorners = (Button)findComponentById(ResourceTable.Id_rounded_corners);
        mRoundedCornersImageCropping = (Button)findComponentById(ResourceTable.Id_rounded_corners_image_cropping);
        mImageCropping.setClickedListener(this);
        mImageCroppingNetwork.setClickedListener(this);
        mRoundedCorners.setClickedListener(this);
        mRoundedCornersImageCropping.setClickedListener(this);
        ohos.global.resource.ResourceManager resManager = this.getResourceManager();
        int mcolor = 0;
        try {
            mcolor = resManager.getElement(ResourceTable.Color_myblue).getColor();
        } catch (IOException e) {
            e.getMessage();
        } catch (NotExistException e) {
            e.getMessage();
        } catch (WrongTypeException e) {
            e.getMessage();
        }
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(mcolor); // 设置状态栏颜色
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_image_cropping:
                present(new ImageCroppingAbilitySlice(),new Intent());
                break;
            case ResourceTable.Id_image_cropping_network:
                present(new NetwordImageCroppingAbilitySlice(),new Intent());
                break;
            case ResourceTable.Id_rounded_corners:
                present(new RoundedCornerImagesAbilitySlice(),new Intent());
                break;
            case ResourceTable.Id_rounded_corners_image_cropping:
                Intent intent = new Intent();
                intent.setParam("radio",radios);
                present(new NetwordImageCroppingAbilitySlice(),intent);
                break;
            default:
                break;
        }
    }
}
