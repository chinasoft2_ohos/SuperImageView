/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.slice;

import com.codeforvictory.superimageview.samples.superimageview.ResourceTable;
import com.codeforvictory.superimageview.samples.superimageview.provider.ImageProvider;
import com.codeforvictory.superimageview.samples.superimageview.shared.ImageLocalDataSource;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.ComponentContainer;

/**
 * RoundedCornerImagesAbilitySlice
 *
 * @author AnBetter
 * @since 2021-05-11
 */
public class RoundedCornerImagesAbilitySlice extends AbilitySlice {
    private ListContainer mListContainer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer)
                LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_images, null, false);
        setUIContent(rootLayout);
        mListContainer = (ListContainer) rootLayout.findComponentById(ResourceTable.Id_listimages);
        ImageProvider imageProvider = new ImageProvider(rootLayout.getContext(),ImageLocalDataSource.networkImages());
        mListContainer.setItemProvider(imageProvider);
    }
}
