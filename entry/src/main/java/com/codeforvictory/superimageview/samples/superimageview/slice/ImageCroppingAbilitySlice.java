/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.slice;

import com.carl.cutimgutil.NineCropType;
import com.carl.cutimgutil.model.LocalImage;
import com.codeforvictory.superimageview.samples.superimageview.ResourceTable;
import com.codeforvictory.superimageview.samples.superimageview.provider.ViewPagerProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;
import java.util.ArrayList;
import java.util.List;

/**
 * ImageCroppingAbilitySlice
 *
 * @author AnBetter
 * @since 2021-04-25
 */
public class ImageCroppingAbilitySlice extends AbilitySlice {
    private PageSlider viewPager;
    private ViewPagerProvider pagerProvider;
    private List<LocalImage> imageData = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_image_cropping);
        viewPager = (PageSlider) findComponentById(ResourceTable.Id_viewPager);
        imageData.add(new LocalImage(NineCropType.NONE, "Original horizontal image",
                ResourceTable.Media_ball_horizontal));
        imageData.add(new LocalImage(NineCropType.NONE, "Original vertical image",
                ResourceTable.Media_ball_vertical));
        imageData.add(new LocalImage(NineCropType.TOP_LEFT, "Left Top crop of the horizontal image",
                ResourceTable.Media_ball_horizontal));
        imageData.add(new LocalImage(NineCropType.TOP, "Center Top crop of the vertical image",
                ResourceTable.Media_ball_vertical));
        imageData.add(new LocalImage(NineCropType.TOP_RIGHT, "Right Top crop of the horizontal image",
                ResourceTable.Media_ball_horizontal));
        imageData.add(new LocalImage(NineCropType.LEFT, "Left Center crop of the horizontal image",
                ResourceTable.Media_ball_horizontal));
        imageData.add(new LocalImage(NineCropType.RIGHT, "Right Center crop of the horizontal image",
                ResourceTable.Media_ball_horizontal));
        imageData.add(new LocalImage(NineCropType.BOTTOM_LEFT, "Left Bottom crop of the horizontal image",
                ResourceTable.Media_ball_horizontal));
        imageData.add(new LocalImage(NineCropType.BOTTOM, "Center Bottom crop of the vertical image",
                ResourceTable.Media_ball_vertical));
        imageData.add(new LocalImage(NineCropType.BOTTOM_RIGHT, "Right Bottom crop of the horizontal image",
                ResourceTable.Media_ball_horizontal));
        pagerProvider = new ViewPagerProvider(this, imageData);
        viewPager.setProvider(pagerProvider);
    }
}
