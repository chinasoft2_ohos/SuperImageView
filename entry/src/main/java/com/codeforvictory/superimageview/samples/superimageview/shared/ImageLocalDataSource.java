/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.shared;

import com.carl.cutimgutil.model.LocalImage;
import com.codeforvictory.superimageview.samples.superimageview.ResourceTable;
import java.util.Arrays;
import java.util.List;

/**
 * ImageLocalDataSource
 *
 * @author AnBetter
 * @since 2021-05-11
 */
public final class ImageLocalDataSource {
    private static String imageUrl = "https://images.unsplash.com/photo-1512061203001-0c4d837c6882?w=600";
    private static String histroryImageUrl = "https://images.pexels.com/photos/161798/stonehenge-"
            + "architecture-history-monolith-161798.jpeg?auto=compress&h=400";
    private static final LocalImage[] LOCALIMAGES = {
        new LocalImage(CropType.NONE, "Original horizontal image",
                ResourceTable.Media_ball_horizontal),
        new LocalImage(CropType.NONE, "Original vertical image", ResourceTable.Media_ball_vertical),
        new LocalImage(CropType.TOP_LEFT, "Left Top crop of the horizontal image", ResourceTable.Media_ball_horizontal),
        new LocalImage(CropType.TOP, "Center Top crop of the vertical image", ResourceTable.Media_ball_vertical),
        new LocalImage(CropType.TOP_RIGHT, "Right Top crop of the horizontal image",
                ResourceTable.Media_ball_horizontal),
        new LocalImage(CropType.LEFT, "Left Center crop of the horizontal image",
                ResourceTable.Media_ball_horizontal),
        new LocalImage(CropType.RIGHT, "Right Center crop of the horizontal image",
                ResourceTable.Media_ball_horizontal),
        new LocalImage(CropType.BOTTOM_LEFT, "Left Bottom crop of the horizontal image",
                ResourceTable.Media_ball_horizontal),
        new LocalImage(CropType.BOTTOM, "Center Bottom crop of the vertical image",
                ResourceTable.Media_ball_vertical),
        new LocalImage(CropType.BOTTOM_RIGHT, "Right Bottom crop of the horizontal image",
                ResourceTable.Media_ball_horizontal),
    };
    private static final NetworkImage[] NETWORKIMAGES = {
        new NetworkImage(
          CropType.NONE,
          "Original image (vertical)",
                imageUrl
      ),
        new NetworkImage(
          CropType.TOP,
          "Top crop of the original image",
              imageUrl
      ),
        new NetworkImage(
          CropType.RIGHT,
          "Right crop of the original image",
              imageUrl
      ),
        new NetworkImage(
          CropType.BOTTOM,
          "Bottom crop of the original image",
              imageUrl
      ),
        new NetworkImage(
          CropType.LEFT,
          "Left crop of the original image",
              imageUrl
      ),
        new NetworkImage(
          CropType.TOP_LEFT,
          "Top left crop of the original image",
              imageUrl
      ),
        new NetworkImage(
          CropType.TOP_RIGHT,
          "Top right crop of the original image",
              imageUrl
      ),
        new NetworkImage(
          CropType.BOTTOM_RIGHT,
          "Bottom right crop of the original image",
              imageUrl
      ),
        new NetworkImage(
          CropType.BOTTOM_LEFT,
          "Bottom left crop of the original image",
              imageUrl
      ),
        new NetworkImage(
          CropType.NONE,
          "Original image (horizontal)",
              histroryImageUrl
      ),
        new NetworkImage(
          CropType.TOP,
          "Top crop of the original image",
              histroryImageUrl
      ),
        new NetworkImage(
          CropType.RIGHT,
          "Right crop of the original image",
              histroryImageUrl
      ),
        new NetworkImage(
          CropType.BOTTOM,
          "Bottom crop of the original image",
              histroryImageUrl
      ),
        new NetworkImage(
          CropType.LEFT,
          "Left crop of the original image",
              histroryImageUrl
      ),
        new NetworkImage(
          CropType.TOP_LEFT,
          "Top left crop of the original image",
              histroryImageUrl
      ),
        new NetworkImage(
          CropType.TOP_RIGHT,
          "Top right crop of the original image",
              histroryImageUrl
      ),
        new NetworkImage(
          CropType.BOTTOM_RIGHT,
          "Bottom right crop of the original image",
              histroryImageUrl
      ),
        new NetworkImage(
          CropType.BOTTOM_LEFT,
          "Bottom left crop of the original image",
              histroryImageUrl
      ),
    };

    private ImageLocalDataSource() {
        throw new AssertionError("This shouldn't be initialized!");
    }

    /** localImages
     *
     * @return List
     */
    public static List<LocalImage> localImages() {
        return Arrays.asList(LOCALIMAGES);
    }

    /** networkImages
     *
     * @return List
     */
    public static List<NetworkImage> networkImages() {
        return Arrays.asList(NETWORKIMAGES);
    }
}
