/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.shared;

/**
 * LocalImage
 *
 * @author AnBetter
 * @since 2021-05-11
 */
public class NetworkImage {
    private int cropType;
    private String title;
    private String imageUrl;

    NetworkImage(int cropType, String title, String imageUrl) {
        this.cropType = cropType;
        this.title = title;
        this.imageUrl = imageUrl;
    }

    /**
     * cropType
     *
     * @return cropType
     */
    public int cropType() {
        return cropType;
    }

    /**
     * title
     *
     * @return title
     */
    public String title() {
        return title;
    }

    /**
     * imageUrl
     *
     * @return imageUrl
     */
    public String imageUrl() {
        return imageUrl;
    }
}

