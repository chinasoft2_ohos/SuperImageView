/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.shared;

/**
 * CropType
 *
 * @author AnBetter
 * @since 2021-05-11
 */
public interface CropType {
    /**
     * NONE
     */
    int NONE = -1;
    /**
     * TOP_LEFT
     */
    int TOP_LEFT = 0;
    /**
     * LEFT
     */
    int LEFT = 1;
    /**
     * BOTTOM_LEFT
     */
    int BOTTOM_LEFT = 2;
    /**
     * TOP_RIGHT
     */
    int TOP_RIGHT = 3;
    /**
     * RIGHT
     */
    int RIGHT = 4;
    /**
     * BOTTOM_RIGHT
     */
    int BOTTOM_RIGHT = 5;
    /**
     * TOP
     */
    int TOP = 6;
    /**
     * BOTTOM
     */
    int BOTTOM = 7;

    /**
     * test
     */
    void test();
}