/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.provider;

import com.carl.cutimgutil.model.LocalImage;
import com.carl.cutimgutil.util.ImageCutUtils;
import com.codeforvictory.superimageview.samples.superimageview.ResourceTable;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.PixelMap;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.io.IOException;
import java.util.List;

/**
 * 类功能描述(类上)
 *
 * @author fuchi
 * @since 2021-05-11
 */
public class BoatPagerProvider extends BaseItemProvider {
    private Context context;
    private List<LocalImage> networkImages;
    private float radio = 0f;

    /**
     * BoatPagerProvider
     *
     * @param context
     * @param networkImages
     */
    public BoatPagerProvider(Context context, List<LocalImage> networkImages) {
        this.context = context;
        this.networkImages = networkImages;
    }

    @Override
    public int getCount() {
        return networkImages.size();
    }

    @Override
    public Object getItem(int position) {
        return networkImages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_boat_image, null, false);
        } else {
            cpt = convertComponent;
        }
        Image image = (Image) cpt.findComponentById(ResourceTable.Id_item_news_image);
        LocalImage model = networkImages.get(i);
        PixelMap cutMap = null;
        if (model.getImgUrl() != null && model.getImgUrl().length() > 0) {
            try {
                cutMap = ImageCutUtils.cuthThreePic(model, image, context);
            } catch (IOException e) {
                e.getMessage();
            } catch (NotExistException e) {
                e.getMessage();
            }
        } else {
            try {
                cutMap = ImageCutUtils.cutvThreePic(model, image, context);
            } catch (IOException e) {
                e.getMessage();
            } catch (NotExistException e) {
                e.getMessage();
            }
        }
        image.setPixelMap(cutMap);
        image.setCornerRadius(radio);
        return cpt;
    }

    public void setRadios(float radios) {
        this.radio = radios;
    }
}
