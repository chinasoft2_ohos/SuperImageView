/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.provider;

import com.bumptech.glide.Glide;
import com.carl.cutimgutil.model.LocalImage;
import com.carl.cutimgutil.util.ImageCutUtils;
import com.codeforvictory.superimageview.samples.superimageview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;

import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Text;
import ohos.global.resource.NotExistException;
import ohos.media.image.PixelMap;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Image;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ImageProvider
 *
 * @author AnBetter
 * @since 2021-05-11
 */
public class ViewPagerProvider extends PageSliderProvider {
    private List<LocalImage> imageData = new ArrayList<>();
    private AbilitySlice mSlice;

    /** ViewPagerProvider
     *
     * @param slice
     * @param data
     */
    public ViewPagerProvider(AbilitySlice slice, List<LocalImage> data) {
        mSlice = slice;
        imageData = data;
    }

    @Override
    public int getCount() {
        return imageData.size();
    }

    /**
     * 创建页面
     *
     * @param componentContainer
     * @param i
     * @return Object
     */
    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        final Component cpt;
        if (componentContainer.getComponentAt(i) == null) {
            cpt = LayoutScatter.getInstance(mSlice).parse(ResourceTable.Layout_item_image_show, null, false);
            componentContainer.addComponent(cpt);
        } else {
            cpt = componentContainer.getComponentAt(i);
        }
        LocalImage showData = imageData.get(i);
        Image image = (Image) cpt.findComponentById(ResourceTable.Id_item_image);
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_item_name);
        PixelMap cutMap = null;
        try {
            cutMap = ImageCutUtils.cutNinePic(showData, image, mSlice);
        } catch (IOException e) {
            e.getMessage();
        } catch (NotExistException e) {
            e.getMessage();
        }
        Glide.with(mSlice)
                .asDrawable()
                .load(cutMap)
                .into(image);
        text.setText(showData.getTitle());
        return cpt;
    }

    /**
     * 销毁页面
     *
     * @param componentContainer
     * @param i
     * @param o
     */
    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    /**
     * 页面匹配
     *
     * @param component 组件
     * @param mobject
     * @return boolean
     */
    @Override
    public boolean isPageMatchToObject(Component component, Object mobject) {
        return component == mobject;
    }
}
