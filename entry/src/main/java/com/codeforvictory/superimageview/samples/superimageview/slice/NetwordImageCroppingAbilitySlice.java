/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codeforvictory.superimageview.samples.superimageview.slice;

import com.carl.cutimgutil.NineCropType;
import com.carl.cutimgutil.model.LocalImage;
import com.codeforvictory.superimageview.samples.superimageview.ResourceTable;
import com.codeforvictory.superimageview.samples.superimageview.provider.BoatPagerProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * NetwordImageCroppingAbilitySlice
 *
 * @author AnBetter
 * @since 2021-04-25
 */
public class NetwordImageCroppingAbilitySlice extends AbilitySlice {
    private ListContainer listContainer;
    private BoatPagerProvider pagerProvider;
    private List<LocalImage> imageData = new ArrayList<>();
    private String url = "https://images.pexels.com/photos/161798/stonehenge-"
            + "architecture-history-monolith-161798.jpeg?auto=compress&h=400";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_images);
        float radios = 0f;
        if (intent != null) {
            radios = intent.getFloatParam("radio", 0f);
        }
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_listimages);
        imageData.add(new LocalImage(NineCropType.NONE, "", ResourceTable.Media_chuan));
        imageData.add(new LocalImage(NineCropType.BOAT_TOP, "", ResourceTable.Media_chuan));
        imageData.add(new LocalImage(NineCropType.BOAT_CENTER, "", ResourceTable.Media_chuan));
        imageData.add(new LocalImage(NineCropType.BOAT_BOTTOM, "", ResourceTable.Media_chuan));
        imageData.add(new LocalImage(NineCropType.NONE, "", ResourceTable.Media_mount));
        imageData.add(new LocalImage(NineCropType.MOUNT_LEFT, "", ResourceTable.Media_mount, url));
        imageData.add(new LocalImage(NineCropType.MOUNT_CENTER, "", ResourceTable.Media_mount, url));
        imageData.add(new LocalImage(NineCropType.MOUNT_RIGHT, "", ResourceTable.Media_mount, url));
        pagerProvider = new BoatPagerProvider(this, imageData);
        pagerProvider.setRadios(radios);
        listContainer.setItemProvider(pagerProvider);
    }
}
