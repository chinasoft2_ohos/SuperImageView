# SuperImageView

#### 项目介绍
- 项目名称：SuperImageView图片裁剪
- 所属系列：openharmony的第三方组件适配移植
- 功能：无论图像大小如何，我们都需要在某些地方裁剪图像，支持网络图片裁剪。
- 基线版本：master
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1


#### 效果演示

<img src="gif/SuperImageViewDemo.gif"></img>


#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
 ```
2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:SuperImageView:1.0.0')
    ......  
 }
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

**加载glide用于网络图片加载**
```xml
dependencies {
   implementation 'io.openharmony.tpc.thirdlib:glide:1.0.4'
   ......  
}
```

**网络图片加载**
```xml
 Glide.with(context)
      .asDrawable()
      .load(model.imageUrl())
      .into(aiv);
```

**对横向或纵向图片图片进行九宫格方式裁剪**
```xml
ImageCutUtils.cutNinePic(LocalImage imdel, Image image, Context context)
```

**对纵向图片进行三等分裁剪**
```xml
ImageCutUtils.cutvThreePic(LocalImage imdel, Image image, Context context)
```
**对横向图片进行三等分裁剪**
```xml
ImageCutUtils.cuthThreePic(LocalImage imdel, Image image, Context context)
```

**对裁剪图片进行圆角设置**
```xml
pagerProvider.setRadios(radios);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

```
The MIT License (MIT)

Copyright (c) 2016 César Díez Sánchez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```