## 1.0.0
ohos 第三个版本
- 正式版本
## 0.0.2-SNAPSHOT

ohos 第二个版本,更新SDK6

## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 原组件ImageCroppingActivity中Button显示RoundedCorners点击进入Rounded CornerImagesActivity页面并未显示圆角图片，鸿蒙组件修改圆角图片显示
 * ImageCroppingActivity中Left Top crop of the horizontal image、Left Center crop of the horizontal image、Left Top crop of the horizontal image，发现三种裁剪和显示方式都是一样的。没有任何区别。鸿蒙组件修改为九宫格裁剪方式
